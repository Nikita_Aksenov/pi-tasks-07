#pragma once
// ������ ������ ������ �� ���������� �����������
class Heading
{
public:
	virtual int PayHeading(int pay, int numberEmp) = 0; 
};
// ������ ������ ������ �� ������������ ������� � �������
class Project
{
public:
	virtual int PayProject(int budget, double ratio) = 0; 
};
// ������ ������ ������ �� ������������� �������
class Worktime
{
public:
	virtual int PayTime(int time, int base) = 0; 
};