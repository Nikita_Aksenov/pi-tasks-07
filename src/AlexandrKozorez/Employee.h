#pragma once

#include "stdc++.h"
#include "Interfaces.h"
using namespace std;

class Employee {

	protected:
		int id, worktime;
		double payment;
		string name;
	public:
		Employee(int id=0, string name="", int worktime=0) 
		{
			this->id = id;
			this->name = name;
			this->worktime = worktime;
			payment = 0;
		}
		~Employee(){}
		int getID() const{ return id; }
		string getName() const{ return name; }
		int getPayment() const{ return payment; }
		int getWorktime() const{ return worktime; }
		void setID(int id) { this->id = id; }
		void setWorktime(int worktime) { this->worktime = worktime; }
		void setName(string name) { this->name = name; }
		virtual void calculate() = 0;
};